%!TEX output_directory=output

\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}

\title{Laying out images with size constraints}
\author{Michel Beaudouin-Lafon}
\date{21 March 2020}

\begin{document}
\maketitle


%================================================================================
\section{Laying out a row of images}
\label{onerow}
%================================================================================

Let us assume a row of $n$ images $I_1 .. I_n$, with widths $w_i$ and heights $h_i$, determining aspect ratios $r_i = w_i / h_i$ (Figure~\ref{fig:onerow}).

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{images/row.pdf}
\caption{Laying out images in a row}
\label{fig:onerow}
\end{figure}

\subsection{Single-row layout for a given width}
\label{onerow-noconstraint}
%--------------------------------------------------------------------------------
We want to create a row with these images so they all have the same height $H$ and fit in a space of width $W$.
We also want to have margins $m_i$ ($i=1..n-1$) between the images.

The given are the aspect ratios $r_i$, the margins $m_i$ and the overall width $W$.
The unknowns are the common height $H = h_1 = .. = h_n$ and the corresponding widths of each image $w_i = r_i * h_i$

The overall width of the images must satisfy 
\begin{equation}
	W = \sum_{i=1}^n w_i + M \textrm{ where } M = \sum_{i=1}^{n-1} m_i
\end{equation}

By replacing $w_i$ by $r_i * h_i$ and noting that $h_i = H$ for all images, we get:
\begin{equation}
	W = H \sum_{i=1}^n r_i + M
\end{equation}

We define $R = \sum_{i=1}^n r_i$ and solve for H, giving:
\begin{equation}\label{eq:single-row-H}
	\boxed{H = (W - M) / R}
\end{equation}

\subsection{Single-row layout for a given width and height}
\label{onerow-fixedheight}
%--------------------------------------------------------------------------------
We have a fixed width \textit{and} a fixed height and want to spread the space between the images. 
From equation~\ref{eq:single-row-H}, we calculate the space $M'$ to be distributed among the images:
\begin{equation}
	M' = W - H*R
\end{equation}

Let us call $M_\mathit{min}$ the minimum overall margins between images (it can be set to 0). 

If $M' \geq M_\mathit{min}$ the images won't overlap and the space can be evenly distributed between the images: $m'_i = M' / (n-1)$.

Alternatively, if the given margins $m_i$ between each image are interpreted as weights, we can divide the available space $M'$ according to these weights: $m'_i = m_i * M'/M$ 

If $M < M_\mathit{min}$ the images overlap. 
We can calculate a horizontal crop factor to adjust the widths of each image so they don't overlap.
We need to calculate new widths $w'_i$ such that 
\begin{equation}
	\sum_{i=1}^n w'_i = W - M_\mathit{min}
\end{equation}

The simplest way is to reduce images proportionally to their size:
\begin{equation}\label{eq:crop-width}
	\boxed{w'_i = k * w_i \textrm{ where } k = (W - M_\mathit{min}) / \sum_{i=1}^n w_i}
\end{equation}


\subsection{Single-row layout with a height constraint}
\label{onerow-constrainedheight}
%--------------------------------------------------------------------------------
We can combine the two previous methods by setting a minimum and maximum height, $H_\mathit{min} and H_\mathit{max}$ for the images.

We use the first method to calculate $H$ (equation~\ref{eq:single-row-H}). 
If $H_\mathit{min} < H < H_\mathit{max}$, we are done.
Otherwise we use the second method with $H = H_\mathit{min}$ or $H = H_\mathit{max}$ depending on the case.


%================================================================================
\section{Laying out a column of images}
\label{column}
%================================================================================

The previous results trivially convert to a vertical layout, by swapping widths and heights.

To calculate the common width $W = w_i$ of images stacked vertically with a given overall height $H$:
\begin{equation}
	\boxed{W = h_i = (H - M) / \sum_{i=1}^n 1/r_i}
\end{equation}

To crop the heights of images given $W$ and $H$ and a minimum overall vertical margin $M_\mathit{min}$:
\begin{equation}\label{eq:crop-height}
	\boxed{h'_i = h_i * (H - M_\mathit{min}) / \sum_{i=1}^n h_i}
\end{equation}


%================================================================================
\section{Laying out a set of columns}
\label{columns}
%================================================================================

We now generalize the previous approach to the layout of a series of columns of images (Figure~\ref{fig:columns}).
The columns are separated by margins, and the overall layout must have a set width of $W$.
The heights of all the columns must be the same (unknown) value $H$.

Let us assume $n$ columns of $n_i$ images each. $I_{i,j}$ is the $j$-th image on column $i$. It has width $w_{i,j}$, height $h_{i,j}$ and aspect ratio $r_{i,j}$.
The images on column $i$ are separated by margins $m_{i,j}$ for $j = 1 .. n_i -1$.
The columns are separated by margins $l_i$ for $i = 1 .. n-1$.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{images/columns.pdf}
\caption{Laying out images in multiple columns}
\label{fig:columns}
\end{figure}


\subsection{Column layout for a given width}
\label{columns-noconstraint}
%--------------------------------------------------------------------------------
The height of each column is:
\begin{equation}
	H_i = H = \sum_{j=1}^{n_i} h_{i,j} + M_i \textrm{ where } M_i = \sum_{j=1}^{n_i-1} m_{i,j}
\end{equation}

All images in a column must ave the samewidth $W_i$, so $r_{i,j} = w_{i,j}/h_{i,j} = W_i/h_{i,j}$.
Therefore $h_{i,j} = W_i,/r_{i,j}$ and we can rewrite the above as:
\begin{equation}
	H = W_i \sum_{j=1}^{n_i} 1/r_{i,j} + M_i
\end{equation}

Let us note $R_i = \sum_{j=1}^{n_i} 1/r_{i,j}$. 
We then have $n$ equations where the unknowns are the $W_i$ and $H$:
\begin{equation}\label{eq:Wi}
	R_i W_i -H + M_i = 0
\end{equation}

Now let us express the overall width of the layout:
\begin{equation}
	W = \sum_{i=1}^n W_i + L \textrm{ where } L = \sum_{i=1}^{n-1} l_i,
\end{equation}

We replace $W_i$ by $(H - M_i) / R_i$ (from equation~\ref{eq:Wi}) to get
\begin{equation}
	W = \sum_{i=1}^n (H - M_i)/R_i + L = H \sum_{i=1}^n 1/R_i - \sum_{i=1}^n M_i/R_i + L
\end{equation}

Solving for $H$, we get:
\begin{equation}
	\boxed{H = \frac{W - L + \sum_{i=1}^n \frac{M_i}{R_i}}{\sum_{i=1}^n \frac{1}{R_i}}}
\end{equation}

Substituting $H$ back in equation~\ref{eq:Wi} we can calculate the width $W_i$ of each column:
\begin{equation}
	\boxed{W_i = (H - M_i) / R_i}
\end{equation}

\subsection{Column layout with a height constraint}
\label{columns-constrainedheight}
%--------------------------------------------------------------------------------

Let us now assume that the height calculated above must be kept within certain bounds $[H_\mathit{min}..H_\mathit{max}]$.
Let us call $H'$ this new height.

Horizontally, the space $L'$ to be distributed among the columns is
\begin{equation}
	L' = W - \sum_{i=1}^n W_i
\end{equation}

Let us call $L_\mathit{min}$ the smallest overall column margin (it can be set to 0).

If $L' \geq L_\mathit{min}$, the columns will not overlap and we can scale each column margin: $l'_i = l_i * L'/L$.

If $L < L_\mathit{min}$, the column overlap, so we need to change their widths, which will crop the images.
The new widths are scaled proportionally as follows (see equation~\ref{eq:crop-width}):
\begin{equation}
	\boxed{W'_i = K * W_i \textrm{ where } K = (W - L_\mathit{min}) / \sum_{i=1}^n W_i}
\end{equation}

For each column, we follow a similar process.
The space $M'_i$ to be distributed among the images in column $i$ is
\begin{equation}
	M'_i = H' - W_i\sum_{j=1}^{n_i} 1 / r_{i,j} = H' - W_i * R_i
\end{equation}

Let us call $M_\mathit{min,i}$ the smallest overall vertical margin for column $i$ (it can be set to 0).

If $M'_i \geq M_\mathit{min,i}$, the images will not overlap and we can scale each margin: $m'_{i,j} = m_{i,j} * M'_i/M_i$.

If $M'_i < M_\mathit{min,i}$, the images overlap, so we need to change their heights, which will crop the images.
The new heights are scaled proportionally as follows (see equation~\ref{eq:crop-height}):
\begin{equation}
	\boxed{h'_{i,j} = k_i * h_{i,j} \textrm{ where } k_i = (H' - M_\mathit{min,i}) / \sum_{j=1}^{n_i} h_{i,j}}
\end{equation}


%================================================================================
\section{Laying out a set of rows}
\label{rows}
%================================================================================

We now turn to the layout of a series of rows of images (Figure~\ref{fig:rows}).
Each row must have the same width $W$, and the rows are separate by vertical margins.

We keep the same notations as in the above section \ref{columns}, swapping columns and rows, widths and heights.

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{images/rows.pdf}
\caption{Laying out images in rows columns}
\label{fig:rows}
\end{figure}

\subsection{Row layout for a given width}
\label{rows-noconstraint}
%--------------------------------------------------------------------------------

If there is no constraint on the overall height, we simply use formula~\ref{eq:single-row-H} from section \ref{onerow-noconstraint} for each row.

\subsection{Row layout with a height constraint}
\label{rows-constrainedheight}
%--------------------------------------------------------------------------------

If the overall height is set, or has a min/max value, the height of each row must be calculated.
This is the same problem as in the previous section \ref{columns-constrainedheight}, swapping heights and widths and replacing columns by rows.

The vertical space $L'$ to be distributed among the rows is $L' = H - \sum_{i=1}^n H_i$ where $H_i$ is the height of row $i$, calculated as in section \ref{onerow-noconstraint}.

If $L' \geq L_\mathit{min}$ (minimum total vertical space between rows), the rows will not overlap and we can scale each row margin: $l'_i = l_i * L'/L$.

If $L < L_\mathit{min}$, the rows overlap, so we need to change their heights, which will crop the images.
The new widths are scaled proportionally as follows:
\[\boxed{H'_i = K * H_i \textrm{ where } K = (H - L_\mathit{min}) / \sum_{i=1}^n H_i}\]

For each row, the space $M'_i$ to be distributed among the images in row $i$ is
\[M'_i = W - H_i\sum_{j=1}^{n_i} r_{i,j} = W - H_i * R_i\]

If $M'_i \geq M_\mathit{min,i}$ (minimum total horizontal space in row $i$), the images will not overlap and we can scale each margin: $m'_{i,j} = m_{i,j} * M'_i/M_i$.

If $M'_i < M_\mathit{min,i}$, the images overlap, so we need to change their widths, which will crop the images.
The new widths are scaled proportionally as follows:
\[\boxed{w'_{i,j} = k_i * w_{i,j} \textrm{ where } k_i = (W - M_\mathit{min,i}) / \sum_{j=1}^{n_i} w_{i,j}}\]

\end{document}
