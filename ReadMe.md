# Image Layout
A set of demos showing how to lay out images in rows or columns so they match a given width and an optional height constraint.

## Running the demos
There are 3 demos, which run in any modern browser.
Each demo has a control panel at the top right to control:

- The width of the layout (as a percentage of the window width)
- The margin between images
- The margin between columns (demo 2 only) or between rows (demo 3 only)
- A checkbox to enable/disable a height constraint
- Minimal and maximal height (when the height constraint is enabled)
- Minimal margin between images (when the height constraint is enabled)

The three demos are :

- `row.html`: a single row of images
- `columns.html`: several columns of images
- `rows.html`: several rows of images

All the demos react in real time to changes in the setting as well as window size.

## The math behind it
`image-layout.pdf` explains the maths behind the layout.

## Bonus demo: framing an image
`frame.html` shows a simple interaction, inspired by Apple Keynote, to frame an image.

Drag the image to move it around, use the scrollwheel to change its size, drag the resize handle to change the frame.
The spacebar hides/shows the parts of the image outside the frame.

`pdf.html` shows the same interaction for a PDF image. To run it you need to run `npm install` in order to install the Javascript PDF reader from Mozilla. Then you need to run a web server, e.g.:

```
% python -m SimpleHTTPServer
```

and load the URL `http://localhost:8000/pdf.html`.
